import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Booking } from '../models/Booking';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from '../security/services/authentication.service';
// import { environment } from 'src/environments/environment.prod'; // To be enabled when we get access to the production environment

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private getUrl = environment.apiBaseUrl;

    constructor(private httpClient: HttpClient,private authService: AuthenticationService) { }

    getBookings(): Observable<Booking[]> {
    
      return this.httpClient.get<Booking[]>(this.getUrl).pipe(
        map (result => result)
      )
    }

    saveBooking(newBooking: Booking): Observable<Booking> {

      return this.httpClient.post<Booking>(this.getUrl, newBooking);
    }

    viewBooking(id: number): Observable<Booking> {
      
      return this.httpClient.get<Booking>(`${this.getUrl}/${id}`).pipe(
        map(result => result)
      )
    }

    deleteBooking(id: number): Observable<any> {
      
      return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: "text"})
    
    }
    
} // end of class
