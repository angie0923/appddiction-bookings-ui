import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


import { HomePageComponent } from './components/home-page/home-page.component';
import { AppComponent } from './app.component';
import { EditBookingComponent } from './components/edit-booking/edit-booking.component';
import { ListBookingsComponent } from './components/list-bookings/list-bookings.component';
import { ConfirmationPageComponent } from './components/confirmation-page/confirmation-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { DestinationPageComponent } from './components/destination-page/destination-page.component';
import { LakeTravisComponent } from './components/lake-travis/lake-travis.component';
import { HendersonBeachComponent } from './components/henderson-beach/henderson-beach.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './security/components/login/login.component';
import { LogoutComponent } from './security/components/logout/logout.component';
import { BauthHttpInterceptService } from './security/services/bauth-http-intercept.service';

@NgModule({
  declarations: [
    AppComponent,
    EditBookingComponent,
    ListBookingsComponent,
    HomePageComponent,
    ConfirmationPageComponent,
    NavbarComponent,
    RegisterFormComponent,
    DestinationPageComponent,
    LakeTravisComponent,
    HendersonBeachComponent,
    LoginComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule
  
  ],
  providers: [
    {  
    provide:HTTP_INTERCEPTORS, useClass:BauthHttpInterceptService, multi:true 
  }
],
  bootstrap: [AppComponent],
  
})
export class AppModule { }

