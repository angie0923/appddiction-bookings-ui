import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from 'src/app/models/Booking';
import { BookingService } from 'src/app/services/booking.service';

@Component({
  selector: 'app-edit-booking',
  templateUrl: './edit-booking.component.html',
  styleUrls: ['./edit-booking.component.css']
})
export class EditBookingComponent implements OnInit {

    newBooking: Booking = {
      id: 0,
      origEntryDate:'',
      guestFirstName:'',
      guestLastName: '',
      guestEmail:'',
      guestCellphone:'',
      partyOf: 0,
      locationRequested:'',
      arrivalDate:'',
      departureDate:'',
      status:'',
      lastModifiedDate:'',
      reviewedBy:'',
      lastReviewDate:''

  }

  isEditing: boolean = false;

  constructor(private bookingSvc: BookingService,
    private router: Router,
    private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {

    var today = new Date().toISOString().split('T')[0];
      document.getElementsByName("origEntryDate")[0].setAttribute('min', today);

    var isIdPresent = this.activeRoute.snapshot.paramMap.has("id");

    if (isIdPresent) {
      const id = this.activeRoute.snapshot.paramMap.get("id");
      this.bookingSvc.viewBooking(Number(id)).subscribe(
        (data: Booking) => this.newBooking = data
      )
    }
  } // end of ngOnInIt

  savedBooking() {
    this.bookingSvc.saveBooking(this.newBooking).subscribe(
      data => {
        this.router.navigateByUrl("/bookings");
      }
    )
  }

  deleteBooking(id: number) {
    this.bookingSvc.deleteBooking(id).subscribe(
      data => {
        this.router.navigateByUrl("/bookings")
      }
    )
  }

} // end of class
