import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HendersonBeachComponent } from './henderson-beach.component';

describe('HendersonBeachComponent', () => {
  let component: HendersonBeachComponent;
  let fixture: ComponentFixture<HendersonBeachComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HendersonBeachComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HendersonBeachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
