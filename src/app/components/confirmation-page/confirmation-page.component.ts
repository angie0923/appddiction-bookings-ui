import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/models/Booking';
import { BookingService } from 'src/app/services/booking.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-confirmation-page',
  templateUrl: './confirmation-page.component.html',
  styleUrls: ['./confirmation-page.component.css']
})

export class ConfirmationPageComponent implements OnInit {

  oneBooking: any;
  newBooking: Booking = {
    id: 0,
    origEntryDate:'',
    guestFirstName:'',
    guestLastName: '',
    guestEmail:'',
    guestCellphone:'',
    partyOf: 0,
    locationRequested:'',
    arrivalDate:'',
    departureDate:'',
    status:'',
    lastModifiedDate:'',
    reviewedBy:'',
    lastReviewDate:''

  }
constructor(private bookingSvc: BookingService,
  private router: Router,
  private activeRoute: ActivatedRoute) { }
  
  ngOnInit(): void {
    // this.getBooking()
  }

  // getBooking() {
  //   this.bookingSvc.viewBooking()
  // }

}
