import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from 'src/app/models/Booking';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  username: string = "";

  constructor(protected router: Router,
    protected activeRoute: ActivatedRoute) { }

  ngOnInit(): void {

  }

  userAccess(){
if (this.username=="admin") {
  this.router.navigateByUrl("/bookings")
} else if (this.username=="user") {
  this.router.navigateByUrl("/destination-page")
} else
this.router.navigateByUrl("/home-page")
}

}



