import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewUser } from 'src/app/security/models/NewUser';
import { RegistrationService } from 'src/app/security/services/registration.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  newUser: NewUser = {

    id: 0,
    username: "",
    isAdmin: "no",
    password: ""

  }

  // message: string = ""

  constructor(private router: Router,
    private regService: RegistrationService,
    private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {

    // var isIdPresent = this.activeRoute.snapshot.paramMap.has("id");
  
    //   if (isIdPresent) {

    //     this.message = "User already exists.";

    //   }
  }

  addNewUser() {
    this.regService.saveNewUser(this.newUser).subscribe(
      data => {
        this.router.navigateByUrl("/login");
      }
    )
  }
}