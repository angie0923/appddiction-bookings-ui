import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/models/Booking';
import { BookingService } from 'src/app/services/booking.service';

@Component({
  selector: 'app-list-bookings',
  templateUrl: './list-bookings.component.html',
  styleUrls: ['./list-bookings.component.css']
})
export class ListBookingsComponent implements OnInit {
    // property
    bookings: Booking[] = [];
    currentDate = new Date();

  // bring in our service, create private variable
  constructor(private bookingSvc: BookingService) { }

  ngOnInit(): void {
    this.listBookings();
  }// end of ngOnInit----------------

    // create an observable method that will display the list of our students / data
    listBookings(){
      this.bookingSvc.getBookings().subscribe(
        data => this.bookings = data
      )
    }
    
     // method that will delete the data using the deletePost() method from our service
    deletedBooking(id: number) {
      this.bookingSvc.deleteBooking(id).subscribe(
        data => this.bookings = data
      )
    }

} // end of class
