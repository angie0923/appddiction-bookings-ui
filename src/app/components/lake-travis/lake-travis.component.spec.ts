import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LakeTravisComponent } from './lake-travis.component';

describe('LakeTravisComponent', () => {
  let component: LakeTravisComponent;
  let fixture: ComponentFixture<LakeTravisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LakeTravisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LakeTravisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
