import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from 'src/app/models/Booking';
import { BookingService } from 'src/app/services/booking.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-lake-travis',
  templateUrl: './lake-travis.component.html',
  styleUrls: ['./lake-travis.component.css'],
  providers: [NgbCarouselConfig] 
})
export class LakeTravisComponent implements OnInit {

  images = [1, 2, 3, 4, 5, 6, 7, 9].map((n) => `../../../assets/images/${n}.jpg`);

    newBooking: Booking = {
    id: 0,
    origEntryDate:'',
    guestFirstName:'',
    guestLastName: '',
    guestEmail:'',
    guestCellphone:'',
    partyOf: 1,
    locationRequested:'Lake Travis',
    arrivalDate:'',
    departureDate:'',
    status:'Pending',
    lastModifiedDate:'',
    reviewedBy:'',
    lastReviewDate:''
  }

  isEditing: boolean = false;

  constructor(private bookingSvc: BookingService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    config: NgbCarouselConfig) {
      config.interval = 5000;
      config.wrap = true;
      config.keyboard = false;
      config.pauseOnHover = true;
     }

    ngOnInit(): void {

      var today = new Date().toISOString().split('T')[0];
      document.getElementsByName("origEntryDate")[0].setAttribute('min', today);

      var isIdPresent = this.activeRoute.snapshot.paramMap.has("id");
  
      if (isIdPresent) {
        const id = this.activeRoute.snapshot.paramMap.get("id");
        this.bookingSvc.viewBooking(Number(id)).subscribe(
          (data: Booking) => this.newBooking = data
        )
      }
    } // end of ngOnInIt
  
    savedBooking() {
      this.bookingSvc.saveBooking(this.newBooking).subscribe(
        data => {
          // this.router.navigateByUrl("/confirmation-page");
        }
      )
    }
  
    deleteBooking(id: number) {
      this.bookingSvc.deleteBooking(id).subscribe(
        data => {
          this.router.navigateByUrl("/destination-page")
        }
      )
    }
  

}
