import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditBookingComponent } from './components/edit-booking/edit-booking.component';
import { DestinationPageComponent } from './components/destination-page/destination-page.component';
import { HendersonBeachComponent } from './components/henderson-beach/henderson-beach.component';
import { ListBookingsComponent } from './components/list-bookings/list-bookings.component';
import { LakeTravisComponent } from './components/lake-travis/lake-travis.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { ConfirmationPageComponent } from './components/confirmation-page/confirmation-page.component';
import { LoginComponent } from './security/components/login/login.component';
import { LogoutComponent } from './security/components/logout/logout.component';
import { AuthGuardService } from './security/services/auth-guard.service';
import { AdminCheckService } from './security/services/admin-check.service';


const routes: Routes = [
  {path: 'bookings', component: ListBookingsComponent,canActivate:[AuthGuardService]},
  {path: 'bookings', component: ListBookingsComponent,canActivate:[AdminCheckService]},
  {path: 'edit-booking', component: EditBookingComponent,canActivate:[AuthGuardService]},
  {path: 'list-bookings', component: ListBookingsComponent,canActivate:[AuthGuardService]},
  {path: 'list-bookings', component: ListBookingsComponent,canActivate:[AdminCheckService]},
  {path: 'edit-booking/:id', component: EditBookingComponent,canActivate:[AuthGuardService]},
  {path: '', redirectTo: '/home-page', pathMatch: 'full'},
  {path: 'destination-page', component: DestinationPageComponent,canActivate:[AuthGuardService]},
  {path: 'henderson-beach', component: HendersonBeachComponent,canActivate:[AuthGuardService]},
  {path: 'lake-travis', component: LakeTravisComponent,canActivate:[AuthGuardService]},
  {path: 'home-page', component: HomePageComponent},
  {path: 'register-form', component: RegisterFormComponent},
  {path: 'confirmation-page', component: ConfirmationPageComponent,canActivate:[AuthGuardService]},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent,canActivate:[AuthGuardService]},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
