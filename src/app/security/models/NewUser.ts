export interface NewUser {

    id: number;
    username: string;
    isAdmin: string;
    password: string;
    
}