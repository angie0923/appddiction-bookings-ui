import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = ''
  isAdmin: string = ''
  password: string = ''
  invalidLogin: boolean = false
  message: string = ""

  constructor(private router: Router,
    private loginservice: AuthenticationService) { }

  ngOnInit(): void {
  }

  checkLogin() {
    (this.loginservice.authenticate(this.username, this.password).subscribe(
      _data => {
        this.checkAdmin(this.isAdmin)
        this.invalidLogin = false
      },
      _error => {
        this.invalidLogin = true

      }
    )
    );
  }

  checkAdmin(isAdmin: string) {
    if (isAdmin === "yes") {
        this.router.navigate(['list-bookings'])
      }
      else {
        this.router.navigate(['destination-page'])
      }
  }

}
