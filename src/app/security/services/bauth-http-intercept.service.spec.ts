import { TestBed } from '@angular/core/testing';

import { BauthHttpInterceptService } from './bauth-http-intercept.service';

describe('BauthHttpInterceptService', () => {
  let service: BauthHttpInterceptService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BauthHttpInterceptService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
