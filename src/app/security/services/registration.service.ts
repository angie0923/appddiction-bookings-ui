import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { NewUser } from '../models/NewUser';
// import { environment } from 'src/environments/environment.prod'; // To be enabled when we get access to the production environment

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  private regUrl = environment.regUrl;

  constructor(private httpClient:HttpClient) {}

    saveNewUser(newUser: NewUser): Observable<NewUser> {

      return this.httpClient.post<NewUser>(this.regUrl, newUser);
    }
    
} // end of class
