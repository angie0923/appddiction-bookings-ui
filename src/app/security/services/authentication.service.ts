import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
export class User{

  constructor(public status: string) {}
  
}

export class JwtResponse{

  constructor(public jwttoken: string,) {}
  
}
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private authUrl = environment.authUrl;

  constructor(private httpClient:HttpClient) {}

  authenticate(username: string, password: string) {
    return this.httpClient.post<any>(this.authUrl,{username, password}).pipe(
     map(
       userData => {
        sessionStorage.setItem("username",username);
        let tokenStr= "Bearer "+userData.token;
        // console.log(tokenStr);
        sessionStorage.setItem("token", tokenStr);
        return userData;
       }
     )

    );
  }


isUserLoggedIn() {
  let user = sessionStorage.getItem("username")
  console.log(!(user === null))
  return !(user === null)
}

isUserAdmin() {
  let admin = sessionStorage.getItem("isAdmin")
  console.log(!(admin === "no"))
  return !(admin === "no")
}

logOut() {
  sessionStorage.removeItem("username")
}
}
