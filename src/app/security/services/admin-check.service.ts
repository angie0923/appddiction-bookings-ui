import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AdminCheckService implements CanActivate {

  constructor(private router: Router,
    private authService: AuthenticationService) { }

  canActivate(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot) {
    if (this.authService.isUserAdmin())
      return true;

    this.router.navigate(['destination-page']);
    return false;

  }

}
