export interface Booking {

    id: number;

    origEntryDate: string;

    guestFirstName: string;

    guestLastName: string;

    guestEmail: string;

    guestCellphone: string;

    partyOf: number;

    locationRequested: string;

    arrivalDate: string;

    departureDate: string;

    status: string;

    lastModifiedDate: string;

    reviewedBy: string;

    lastReviewDate: string;
    
}