import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../security/services/authentication.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  loggedIn: boolean = false;

  constructor(private loginService:AuthenticationService) { }

  ngOnInit(): void {

    this.loggedIn = this.loginService.isUserLoggedIn();
  }

}
