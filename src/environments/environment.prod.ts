export const environment = {
  production: true,
  apiBaseUrl: 'http://localhost:8080/api/v1/bookings', // Will need to be changed to actual production server url
  authUrl: 'http://localhost:8080/authenticate', // Will need to be changed to actual production server auth url
  regUrl: 'http://localhost:8080/register' // Will need to be changed to actual production server reg url
};
